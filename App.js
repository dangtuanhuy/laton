import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import HomeScreen from './src/screens/Home';
import Screens from './src/screens/Screens';
import ListScreen from './src/screens/ListScreen';
import ImgeScreen from './src/screens/ImagesScreen';
import CounterScreen from './src/screens/CounterScreen';
import ColorScreen from './src/screens/ColorScreen';
import SquareScreen from './src/screens/SquareScreen';
const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    Screens: Screens,
    ListScreen: ListScreen,
    ImgeScreen: ImgeScreen,
    CounterScreen: CounterScreen,
    ColorScreen: ColorScreen,
    SquareScreen: SquareScreen,
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      title: 'App',
    },
  },
);

export default createAppContainer(navigator);
