/* eslint-disable prettier/prettier */
// eslint-disable-next-line no-unused-vars
import { Text, StyleSheet, Button, View, Image } from 'react-native';
import React, { Component } from 'react'

export class ImagesDetails extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <View>
                <Image source={this.props.imageSource} />
                <Text>{this.props.title}</Text>
                <Text>Image Score - {this.props.ImageScore}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({});
export default ImagesDetails;
