/* eslint-disable prettier/prettier */
/* eslint-disable no-shadow */
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
class listScreen extends Component {
  render() {
    const friend = [
      { name: 'HACHI 1', age: 10 },
      { name: 'HACHI 2', age: 10 },
      { name: 'HACHI 3', age: 10 },
      { name: 'HACHI 4', age: 10 },
      { name: 'HACHI 5', age: 10 },
      { name: 'HACHI 6', age: 10 },
      { name: 'HACHI 7', age: 10 },
      { name: 'HACHI 8', age: 10 },
      { name: 'HACHI 9', age: 10 },
      { name: 'HACHI 10', age: 10 },
      { name: 'HACHI 11', age: 10 },
    ];
    return (
      <View>
        <FlatList
          keyExtractor={friend => friend.name}
          data={friend}
          renderItem={({ item }) => {
            return (
              <Text style={styles.textStyles}>
                {item.name} - Age {item.age}
              </Text>
            );
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  textStyles: {
    marginVertical: 50,
  },
});
export default listScreen;
