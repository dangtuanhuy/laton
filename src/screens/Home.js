/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { Text, StyleSheet, View, Button } from 'react-native';

class Home extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View>
        <Text style={styles.text}>Hi there</Text>
        <Button title="Go to component" onPress={() => this.props.navigation.navigate('Screens')} />
        <Button title="List Screen" onPress={() => this.props.navigation.navigate('ListScreen')} />
        <Button title="Image Screen" onPress={() => this.props.navigation.navigate('ImgeScreen')} />
        <Button title="Counter Screen" onPress={() => this.props.navigation.navigate('CounterScreen')} />
        <Button title="Color Screen" onPress={() => this.props.navigation.navigate('ColorScreen')} />
        <Button title="Square Screen" onPress={() => this.props.navigation.navigate('SquareScreen')} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontSize: 30,
  },
});

export default Home;
