/* eslint-disable eslint-comments/no-unused-disable */
/* eslint-disable react/jsx-no-undef */
/* eslint-disable prettier/prettier */
import { View, StyleSheet, Image, Text } from 'react-native';
import React, { Component } from 'react';
import ImagesDetails from '../components/ImagesDetails';
class ImagesScreen extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <View>
                <ImagesDetails title="Forest" imageSource={require('../assets/forest.jpg')} ImageScore={9} />
                <ImagesDetails title="Beach" imageSource={require('../assets/beach.jpg')} ImageScore={10}/>
                <ImagesDetails title="Mountain" imageSource={require('../assets/mountain.jpg')} ImageScore={11}/>
            </View>
        );
    }
}
const styles = StyleSheet.create({});
export default ImagesScreen;
